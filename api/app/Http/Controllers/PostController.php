<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\PostRepositoryInterface;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Post;

class PostController extends Controller
{
    private $post;

    public function __construct(PostRepositoryInterface $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => $this->post->all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);
        if (!$validator->fails()) {
            $return['data'] = $this->post->find($this->post->save($request->all()));
            return response()->json($return, 200);
        } else {
            return response()->json([
                    'messages' => 'The given data was invalid',
                    'errors' => $validator->messages()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return response()->json(['data' => compact('post')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $return['data'] = $this->post->find($this->post->update($post, $request->all()));
        return response()->json($return, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->post->delete($post);
        return response()->json(['status' => 'record deleted succesfully']);
    }
}
