<?php
declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Models\Post;
use App\Repository\PostRepositoryInterface;
use Illuminate\Support\Collection;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    /**
     * PostRepository constructor.
     * 
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * @return Int
     */
    public function save(array $request): Int
    {
        $data = $this->model;
        $data->user_id = 1;
        $data->title = $request['title'];
        $data->content = $request['content'];
        $data->image = $request['image'];
        $data->slug = str_replace(' ', '-', $request['title']);
        $data->save();

        return $data->id;
    }

    /**
     * @return Int
     */
    public function update(Post $post, array $request): Int
    {
        $update = $post;
        $update->user_id = 1;
        $update->title = $request['title'] ?? $post->title;
        $update->content = $request['content'] ?? $post->content;
        $update->image = $request['image'] ?? $post->image;
        $update->slug = str_replace(' ', '-', $update->title);
        $update->save();

        return $update->id;
    }

    /**
     * @return Int
     */
    public function delete(Post $post): Int
    {
        return $this->model->destroy($post->id);
    }
}