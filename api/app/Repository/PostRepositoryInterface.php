<?php
declare(strict_types=1);

namespace App\Repository;

use App\Models\Post;
use Illuminate\Support\Collection;

interface PostRepositoryInterface
{
   public function all(): Collection;
   public function save(array $request): Int;
   public function update(Post $post, array $request): Int;
   public function delete(Post $post): Int;
}